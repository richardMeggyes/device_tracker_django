from django.db import models


# class SecurityLogSshAttack(models.Model):
#     """
#     Table that stores the number of ssh attacks per day.
#     """
#     id = models.AutoField(primary_key=True)
#     date = models.DateField(auto_now_add=True, blank=True)
#     number = models.IntegerField(default=0, blank=True, null=False)
#
#     class Meta:
#         db_table = 'security_log_ssh_attack'