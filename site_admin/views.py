from utils import render
# from .tasks import failed_login_attempts_logger
import json
import site_admin.utils as utils

# This is an automated task, and totally intentionally put here. Do not remove it!
# failed_login_attempts_logger(repeat=86400)


def admin_index(request):

    # Getting failed ssh login attempt data for the header
    attempt_numbers_list, attempt_date_list = utils.get_failed_ssh_data()

    return render(request, 'admin/admin_index.html',
                  {
                      'attempt_numbers_list': json.dumps(attempt_numbers_list),
                      'attempt_date_list': json.dumps(attempt_date_list)
                  })


def admin_ssh_log(request):

    # Getting failed ssh login attempt data for the header
    attempt_numbers_list, attempt_date_list = utils.get_failed_ssh_data()

    return render(request, 'admin/header_chart.html',
                  {
                      'attempt_numbers_list': json.dumps(attempt_numbers_list),
                      'attempt_date_list': json.dumps(attempt_date_list)
                  })
