import time, platform, os
from datetime import datetime, timedelta
from background_task import background
# from .models import SecurityLogSshAttack

#
# @background()
# def failed_login_attempts_logger():
#     # TODO clean code, repeating code
#     operating_system = platform.system()
#
#     if not operating_system == 'Linux':
#         log_path = os.getcwd() + "/site_admin/auth.log"
#     else:
#         log_path = "/var/log/auth.log"
#
#     day_str = time.strftime('%b %d', time.gmtime(time.time() - 86400))
#     day_str = day_str.replace('0', ' ')
#     failed_attempts = []
#
#     with open(log_path, "r") as f:
#         lines = f.readlines()
#
#     for line in lines:
#         if "Failed password" in line:
#             if day_str in line:
#                 failed_attempts.append(line)
#
#     failed_attempts = len(failed_attempts)
#     yesterday = datetime.today() - timedelta(days=1)
#
#     log = SecurityLogSshAttack.objects.create()
#     log.number = failed_attempts
#     log.date = yesterday
#     log.save()
