# from .models import SecurityLogSshAttack
import platform, os, time


def get_failed_login_attempts_today():
    operating_system = platform.system()

    if not operating_system == 'Linux':
        log_path = os.getcwd() + "/site_admin/auth.log"
    else:
        log_path = "/var/log/auth.log"

    day_str = time.strftime('%b %d', time.gmtime(time.time()))
    day_str = day_str.replace('0', ' ')
    failed_attempts_today = []

    with open(log_path, "r") as f:  # TODO need to handle when file is not found
        lines = f.readlines()

    for line in lines:
        if "Failed password" in line:
            if day_str in line:
                failed_attempts_today.append(line)
    return len(failed_attempts_today), day_str


# def get_failed_ssh_data():
#     # This gets the last 10 day's failed login attempts
#     ssh_attempt_log = SecurityLogSshAttack.objects.order_by('date')[:10]
#     attempt_numbers_list = []
#     attempt_date_list = []
#     for entry in ssh_attempt_log:
#         attempt_numbers_list.append(entry.number)
#         attempt_date_list.append(entry.date.strftime('%b %d'))
#
#     # This gets today's attempts and appends to the lists
#     attempts_today, str_today = get_failed_login_attempts_today()
#
#     attempt_numbers_list.append(attempts_today)
#     attempt_date_list.append(str_today)
#
#     return attempt_numbers_list, attempt_date_list
