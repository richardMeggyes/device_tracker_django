from django.urls import path
from . import views
from django.contrib import admin

urlpatterns = [
    path('django/', admin.site.urls),
    path('', views.admin_index, name='adminindex'),
    path('ssh_log/', views.admin_ssh_log, name='adminsshlog')
]
