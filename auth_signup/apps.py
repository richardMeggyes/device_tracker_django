from django.apps import AppConfig


class AuthSignupConfig(AppConfig):
    name = 'auth_signup'
