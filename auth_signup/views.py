from utils import render
from django.contrib.auth.models import User
# from django.contrib.auth.decorators import permission_required, login_required
from django.contrib.auth.models import Permission

def signup(request):
    """
    View function for registration page.
    """
    user = User.objects.create_user('readdeo', 'myemail@crazymail.com', '1234')

    # Update fields and then save again
    user.first_name = 'John'
    user.last_name = 'Citizen'
    user.save()

    if request.method == 'POST':
        user_name = request.POST.get("username", "")
        email = request.POST.get("email", "")
        password = request.POST.get("password", "")

        print('signup post:', user_name, email, password)
        print("post:", request.POST)

        # Create user and save to the database
        user = User.objects.create_user('testsignup', 'myemail@crazymail.com', 'mypassword')

        # Update fields and then save again
        user.first_name = 'John'
        user.last_name = 'Citizen'
        user.save()

    # TODO registration confirmation email handling

    return render(
        request,
        'registration/signup.html',
        context={'nothing': 'nothing'},  # Placeholder context for now
    )


def index_page(request):
    """
    View function for the index page.
    """


