from django.urls import path
from . import views

urlpatterns = [
    path('', views.devices_index, name='devices_index'),
    path('new', views.new_device, name='new_device')
]
