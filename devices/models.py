from django.db import models
from django.contrib.auth.models import User


class Device(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    device_name = models.CharField(max_length=200)
    type = models.CharField(max_length=50)

    class Meta:
        db_table = 'device'
