from django.shortcuts import render
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User


@login_required()
def devices_index(request):

    return render(request, 'devices/devices.html',
                  {'asd': "asd"})


@login_required()
def new_device(request):

    return render(request, 'devices/new_device.html',
                  {'asd': "asd"})
