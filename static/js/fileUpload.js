function autoName() {
    let inputs = document.getElementsByClassName('column-name-edit');
    let counter = 1;
    for (input of inputs) {
        input.value = "column_" + counter.toString();
        counter++;
    }
    check_column_name_errors();
}

function trimText() {
    let inputs = document.getElementsByClassName('column-name-edit');
    for (input of inputs) {
        let text = input.value;
        let first_num = true;
        while (first_num) {
            if (!isNaN(parseInt(text.charAt(0), 10))) {
                text = text.slice(1)
            } else {
                first_num = false;
            }
        }

        while (true){
            let restart = false;
            for (let i = 0; i < text.length; i++) {
                if (!/^[a-zA-Z0-9_]*$/.test(text.charAt(i))) {
                    text = text.slice(0, i) + text.slice(i + 1);
                    restart = true;
                    break;
                }
            }
            if (restart){
                continue;
            }
            break;
        }

        // this shit is skipping characters
        // for (let i = 0; i < text.length; i++) {
        //     let currentCharacter = text.charAt(i);
        //     if (!/^[a-zA-Z0-9_]*$/.test(currentCharacter)) {
        //         text = text.slice(0, i) + text.slice(i + 1);
        //     } else if (currentCharacter === ' ') {
        //         text = text.slice(0, i) + text.slice(i + 1);
        //     }
        // }

        input.value = text;
    }
    check_column_name_errors();
}

function submitTable() {
    if (check_column_name_errors()) {
        document.getElementById('createTable').submit();
    }
}

function check_column_name_errors() {
    let textOk = true;
    let inputs = document.getElementsByClassName('column-name-edit');

    for (input of inputs) {
        let id = input.id;
        let submitButton = document.getElementById("submitButton");
        let div = document.getElementById("div_" + id); // this controls the error message, color
        let icon = document.getElementById("icon_" + id); // this is the error icon
        let help = document.getElementById("help_" + id); // this is the error help message
        console.log("div_" + id);
        let text = input.value;

        if (!/^[a-zA-Z0-9_]*$/.test(text)) {
            submitButton.classList.remove("btn-success");
            submitButton.classList.add("btn-danger");
            div.classList.add("has-danger");
            icon.setAttribute("display", "block");
            help.innerText = "Nem megengedett karakter az oszlopnévben!";
            textOk = false;
        } else if (text.length < 4) {
            submitButton.classList.remove("btn-success");
            submitButton.classList.add("btn-danger");
            div.classList.add("has-danger");
            icon.setAttribute("display", "block");
            help.innerText = "Oszlopnév legyen minimum 4 karakter";
            textOk = false;
        } else if (!isNaN(parseInt(text.charAt(0), 10))) {
            submitButton.classList.remove("btn-success");
            submitButton.classList.add("btn-danger");
            div.classList.add("has-danger");
            icon.setAttribute("display", "block");
            help.innerText = "Oszlopnév nem kezdődhet számmal!";
            textOk = false;
        } else {
            submitButton.classList.add("btn-success");
            submitButton.classList.remove("btn-danger");
            console.log(div);
            div.classList.remove("has-danger");

            icon.setAttribute("display", "none");
            help.innerText = "";
        }
    }
    return textOk;
}

let inputs = document.getElementsByClassName('column-name-edit');
for (input of inputs) {
    input.addEventListener('keyup', function () {
        let ok = check_column_name_errors();
        console.log(ok);
    });
}

check_column_name_errors();

