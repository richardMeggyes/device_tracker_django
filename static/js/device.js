function newDevice() {
    let devicesTableBody = document.getElementById("devicesTableBody");
    let newRow = document.createElement('tr');
    newRow.setAttribute("id", "newDeviceRow");
    let newTd = document.createElement('td');
    let newTd2 = document.createElement('td');
    let newTdWithInput = document.createElement('td');
    newTdWithInput.setAttribute("id", "newDeviceInputTd");
    let newInput = document.createElement('input');
    newInput.setAttribute("id", "newDeviceName");

    newTdWithInput.appendChild(newInput);

    let finishedButton = document.createElement('button');
    finishedButton.classList.add("btn", "btn-sm", "btn-success");
    let checkIcon = document.createElement("i");
    checkIcon.setAttribute("class", "material-icons");
    checkIcon.innerText = "done";
    finishedButton.appendChild(checkIcon);
    let btnDiv = document.createElement("div");
    btnDiv.classList.add("ripple-container");
    finishedButton.appendChild(btnDiv);

    let cancelButton = document.createElement('button');
    cancelButton.classList.add("btn", "btn-sm", "btn-danger");
    let xIcon = document.createElement("i");
    xIcon.setAttribute("class", "material-icons");
    xIcon.innerText = "clear";
    cancelButton.appendChild(xIcon);
    let btnDiv2 = document.createElement("div");
    btnDiv2.classList.add("ripple-container");
    cancelButton.appendChild(btnDiv);

    newTd2.appendChild(finishedButton);
    newTd2.appendChild(cancelButton);

    newRow.appendChild(newTdWithInput);
    newRow.appendChild(newTd);
    newRow.appendChild(newTd2);

    devicesTableBody.insertBefore(newRow, devicesTableBody.firstChild);

    let newDeviceButton = document.getElementById("newDeviceButton");
    newDeviceButton.disabled = true;

    cancelButton.addEventListener("click", function () {
        document.getElementById("newDeviceRow").remove();
        newDeviceButton.disabled = false;
    });

    finishedButton.addEventListener("click", sendNewDevice());
}

function sendNewDevice() {
    let newDeviceInput = document.getElementById("newDeviceName");
    let newDeviceName = newDeviceInput.innerText;
    let newDeviceInputTd = document.getElementById("newDeviceInputTd");
    newDeviceInputTd.removeChild(newDeviceInput);
    newDeviceInputTd.innerText = newDeviceName;
    newDeviceInputTd.removeAttribute("id");

    let xmlhttp = new XMLHttpRequest();
    let url = "new-device";
    let params = "name=" + "";

    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            let response = JSON.parse(this.responseText);
            // do something with response
        }
    };
    xmlhttp.open("GET", url + "?" + params, true);
    xmlhttp.send();
}

//
// let sc = $("#spo0kyD").on( "click", function() {
// console.log("something");
// });
//
// var intervalID = setInterval(function () {
//     let sc = $("#spo0kyD");
//     if (sc != null) {
//         console.log("Spooky creature clicked!" + sc);
//         sc.on( "click", function() {
// console.log("Spooky creature clicked! ON");})
//
//     } else {
//         console.log("Spooky creature not found");
//     }
// }, 2000);
//
// // clearInterval(j);
// // clearInterval(c);
// clearInterval(intervalID);

let notified = false;
var intervalID = setInterval(function () {
    let spookyCreature = document.getElementById("spo0kyD");
    if (spookyCreature != null) {
        if (notified === false){
            notified = true;
        console.log("Spooky creature found! " + spookyCreature);
        var audio = new Audio('http://soundbible.com/grab.php?id=1815&type=mp3');
        audio.play();
        }
    } else {
        notified = false;
        console.log("Spooky creature not found");
    }
}, 250);


//
// $(document).ready(function () {
//     console.log("custom script start");
//     function h(i, a) {
//         return Math.floor(Math.random() * (a - i + 1)) + i
//     }
//
//     function d(m) {
//         clearInterval(j);
//         var l = $("#spo0kyD");
//         l.fadeTo("fast", 1);
//         var i = parseInt(l.css("top").replace("px", "")), o = parseInt(l.css("left").replace("px", ""));
//         o + 195 > $(window).width() && (o = $(window).width() - 195), i + 120 > $(window).height() && (i = $(window).height() - 120), l.css({
//             width: "175px",
//             top: i + "px",
//             left: o + "px"
//         }), l.addClass("ape-popup"), l.html(m + '<br /><p class="list_alert" id="removeMessage" style="cursor:pointer;">bezár</p>'), c = setInterval("$('#spo0kyD').fadeOut('slow', function(){$('#spo0kyD').remove()});", 30000)
//     }
//
//     function b() {
//         clearInterval(j), clearInterval(c), $("#spo0kyD").length > 0 && $("#spo0kyD").remove(), "undefined" != typeof k && k.length > 0 && k.modal("hide")
//     }
//
//     var j, k, g, c;
//     $(document).on("click", "#removeMessage", function (i) {
//         // if (void 0 === i.originalEvent) {
//         //     return alert("Kérlek te kattints ne egy kód."), !1
//         // }
//         var a = $("#spo0kyD");
//         a.fadeOut("slow"), a.remove(), clearInterval(c)
//     }), $(document).on("click", "#spo0kyD > img", function (i) {
//         // if (void 0 === i.originalEvent) {
//         //     return alert("Kérlek te kattints ne egy kód."), !1
//         // }
//         if ("true" === $("#spo0kyD").attr("data-recaptcha")) {
//             k = $("#spookymodal").clone(!0).attr("id", ""), k.find(".modal-title").html("Captcha challenge"), k.find(".modal-footer").remove(), k.find(".modal-body").html('<div id="recaptcha"></div><br /><button class="anyanswer">Mehet</button>'), $(document.body).append(k), k.modal(), g = grecaptcha.render("recaptcha", {
//                 sitekey: "6Lca3_4SAAAAAA7wrqlqzaDQmmfYxXvyIuZPr4fl",
//                 theme: "dark"
//             }), clearInterval(j), $("#spo0kyD").stop(!0)
//         } else {
//             var a = {eventid: $("#spo0kyD").attr("data-uuid"), userid: $("#confg").attr("data-uid")};
//             f.send(JSON.stringify(a))
//         }
//     }), $(document).on("click", ".anyanswer", function (m) {
//         // if (void 0 === m.originalEvent) {
//         //     return alert("Kérlek kattints te."), !1
//         // }
//         var l = null;
//         if ("undefined" == typeof $(".g-recaptcha-response").val()) {
//             return m.preventDefault(), !1
//         }
//         l = $(".g-recaptcha-response").val();
//         var i = {recaptcha: l, eventid: $("#spo0kyD").attr("data-uuid"), userid: $("#confg").attr("data-uid")};
//         f.send(JSON.stringify(i))
//     }), $(document).on("hidden.bs.modal", function () {
//         k.remove()
//     });
//     var f = new WebSocket("wss://spooky.ncore.cc:3001/spooky");
//     f.onopen = function () {
//         b()
//     }, f.onmessage = function (m) {
//         var x = JSON.parse(m.data);
//         switch (x.type) {
//             case"spooky":
//                 b();
//                 var q = 100, r = 100, a = 10, e = $(window).width() - q - 10, w = 10, o = $(window).height() - r - 10,
//                     t = Math.floor(Math.random() * (parseFloat(e) - parseFloat(a) + 1)) + parseFloat(a),
//                     n = Math.floor(Math.random() * (parseFloat(o) - parseFloat(w) + 1)) + parseFloat(w);
//                 $("body").append('<div id="spo0kyD" data-uuid="' + x.eventid + '" data-recaptcha="' + x.recaptcha + '" style="z-index:1000;position:fixed;left:' + t + "px;width:" + q + "px;top:" + n + 'px;"><img style="cursor:pointer;" src="https://static.ncore.cc/static/images/spooky2k18/' + h(1, 29) + '.png" /></div>'), j = setInterval("$('#spo0kyD').fadeOut('slow', function(){$('#spo0kyD').remove()});", 30000);
//                 break;
//             case"spooky-event":
//                 "undefined" != typeof k && k.modal("hide"), x.success ? d(x.text) : b(), "undefined" != typeof g && grecaptcha.reset(g)
//         }
//     }
// });
