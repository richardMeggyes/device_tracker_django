function edit_activer() {
    var input_field = document.getElementsByClassName("personal_detail_item");
    var edit_button = document.getElementById("edit_button");

    for (let i = 0; i < input_field.length; i++) {
        input_field[i].disabled = false;
    }

    edit_button.innerHTML = "MENTÉS";
    edit_button.className = "btn btn-danger float-md-right d-inline-block m-0";
    edit_button.onclick = saver;
}

function saver() {
    document.getElementById("user_details").submit();
}

function edit_abroad_card_activer() {
    var input_field = document.getElementsByClassName("abroad_detail_item");
    var edit_button = document.getElementById("abroad_edit_button");

    for (let i = 0; i < input_field.length; i++) {
        input_field[i].disabled = false;
    }

    edit_button.innerHTML = "MENTÉS";
    edit_button.className = "btn btn-danger";
    edit_button.onclick = abroad_saver;
}

function abroad_saver() {
    document.getElementById("user_abroad_details").submit();
}

function edit_ice_card_activer() {
    var input_field = document.getElementsByClassName("ice_detail_item");
    var edit_button = document.getElementById("ice_edit_button");

    for (let i = 0; i < input_field.length; i++) {
        input_field[i].disabled = false;
    }

    edit_button.innerHTML = "MENTÉS";
    edit_button.className = "btn btn-danger";
    edit_button.onclick = ice_saver;
}

function ice_saver() {
    document.getElementById("user_ice_details").submit();
}


function edit_phone_number(buttonId) {
    var inputClassName = buttonId.replace('_edit', '');
    var input_field = document.getElementsByClassName(inputClassName);
    var edit_button = document.getElementById(buttonId);

    for (let i = 0; i < input_field.length; i++) {
        input_field[i].disabled = false;
    }

    edit_button.className = "btn btn-warning";
    edit_button.onclick = function () {
        phone_number_editor(buttonId);
    };

}

function phone_number_editor(buttonId) {
    console.log('ASD', buttonId);

    let formId = buttonId.replace('_edit', '_form');
    console.log(formId);
    document.getElementById(formId).submit();
}

function delete_phone_number(formId) {

    let phoneEditForm = document.getElementById(formId);

    let inputClassName = formId.replace('_form', '');
    let input_field = document.getElementsByClassName(inputClassName);

    for (let i = 0; i < input_field.length; i++) {
        input_field[i].disabled = false;
    }

    let deleteInputName = formId.replace('_form', '_del_input');
    let deleteInput = document.getElementById(deleteInputName);
    console.log(deleteInputName, deleteInput);
    deleteInput.setAttribute('value', 'True');

    phoneEditForm.submit();
}

function newPhoneNumber() {
    console.log('not implemented');
}