function loadTable(direction, table_id) {
    // let tableId = document.getElementById("table_id").value;
    let searchInput = document.getElementById("search_input");
    let beginsSw = document.getElementById("begins");
    let endsSw = document.getElementById("ends");
    let pageNumInput = document.getElementById("page_num");
    let pageNumber = parseInt(pageNumInput.value);
    let tbody = document.getElementById("tableBody");

    let pageNumberCheck = pageNumber === 1;
    let directionCheck = direction === "p";

    if (!(pageNumberCheck && directionCheck)) {

    while (tbody.hasChildNodes()) {
        tbody.removeChild(tbody.lastChild);
    }

    if (direction === "n") {
        pageNumber = pageNumber + 1
    } else if (direction === "p") {
        pageNumber = pageNumber - 1
    }
    let beginStr = "%";
    let endStr = "%";
    if (beginsSw.hasAttribute("checked")) {
        beginStr = "";
    }

    if (endsSw.hasAttribute("checked")) {
        endStr = "";
    }
    let xmlhttp = new XMLHttpRequest();
    let url = "update";
    let params = "page=" + pageNumber + "&limit=15" +"&table_id=" + table_id + "&search=" + beginStr + searchInput.value + endStr ;

    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            let myArr = JSON.parse(this.responseText);

            for (data of myArr) {
                let row = document.createElement('tr');
                for (key in data) {
                    let dataCell = document.createElement('td');
                    dataCell.innerText = data[key];
                    row.appendChild(dataCell);
                }
                tbody.appendChild(row);
            }
        }
    };
    xmlhttp.open("GET", url + "?" + params, true);
    xmlhttp.send();
    pageNumInput.value = pageNumber;
    let page_num_display = document.getElementById("page_num_display");
    page_num_display.innerHTML = pageNumber.toString();
    }
}
