from django.contrib.auth.models import Permission
from django.shortcuts import render as django_render
from django.utils import translation


def render(request, template_name, context={}, content_type=None, status=None, using=None):

    # user_language = 'hu'
    # translation.activate(user_language)
    # request.session[translation.LANGUAGE_SESSION_KEY] = user_language

    context.update({'permissions': get_permissions(request.user), 'user': request.user})
    return django_render(request, template_name, context=context, content_type=None, status=None, using=None)


def get_permissions(user):
    if not str(user) == 'AnonymousUser':
        user_permissions = list(str(group) for group in user.groups.all())
        user_permissions += [x.name for x in Permission.objects.filter(user=user)]
        return user_permissions
    else:
        return []
