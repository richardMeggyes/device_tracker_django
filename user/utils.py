# from django.contrib.auth.models import User
# # from user.models import CountryInfo, UserInfo, UserIce, UserTask, UserInfoSave, UserIceSave, PhoneNumber, PhoneNumberSave
# import logging
#
#
# def update_user_data(request, user):
#     if 'home_country' in request.POST:
#         logging.debug('User_pesonal_update')
#         update_personal_data(request, user)
#         double_save_personal_data(request, user)
#
#     elif 'abroad_country' in request.POST:
#         logging.debug('User_abroad_update')
#         update_abroad_data(request, user)
#         double_save_abroad_data(request, user)
#
#     elif 'ice_email' in request.POST:
#         logging.debug('User_ice_update')
#         update_ice_data(request, user)
#         double_save_ice_data(request, user)
#
#     elif 'phone_id' in request.POST:
#         if request.POST.get("phone_delete", "") == 'True':
#             delete_phone_number(request, user)
#         else:
#             update_phone_data(request, user)
#             double_save_phone_data(request, user)
#
#
# def delete_phone_number(request, user):
#     PhoneNumber.objects.get(user_id=user, id=int(request.POST.get("phone_id", ""))).delete()
#     print("DELETE SAD")
#
#
# def update_personal_data(request, user):
#     # user_name = request.POST.get("username", "") # Changing username is not allowed
#
#     # auth_user table's info
#     auth_user = User.objects.get(id=user.id)
#     auth_user.email = request.POST.get("email", "")
#     auth_user.first_name = request.POST.get("firstname", "")
#     auth_user.last_name = request.POST.get("lastname", "")
#     auth_user.save()
#
#     # UserProfile table's info
#     user_info, created = UserInfo.objects.get_or_create(user=user)
#     user_info.home_country = request.POST.get("home_country", "")
#     user_info.home_city = request.POST.get("home_city", "")
#     user_info.home_street = request.POST.get("home_street", "")
#     user_info.home_house_no = request.POST.get("home_house_no", "")
#     user_info.home_postcode = request.POST.get("home_postcode", "")
#     user_info.save()
#
#
# def update_abroad_data(request, user):
#     user_info, created = UserInfo.objects.get_or_create(user=user)
#     user_info.abroad_country = request.POST.get("abroad_country", "")
#     user_info.abroad_city = request.POST.get("abroad_city", "")
#     user_info.abroad_street = request.POST.get("abroad_street", "")
#     user_info.abroad_house_no = request.POST.get("abroad_house_no", "")
#     user_info.abroad_postcode = request.POST.get("abroad_postcode", "")
#     user_info.save()
#
#
# def update_ice_data(request, user):
#     user_ice, created = UserIce.objects.get_or_create(user=user)
#     user_ice.email = request.POST.get("ice_email", "")
#     user_ice.firstname = request.POST.get("ice_firstname", "")
#     user_ice.lastname = request.POST.get("ice_lastname", "")
#     user_ice.country = request.POST.get("ice_country", "")
#     user_ice.city = request.POST.get("ice_city", "")
#     user_ice.street = request.POST.get("ice_street", "")
#     user_ice.house_no = request.POST.get("ice_house_no", "")
#     user_ice.postcode = request.POST.get("ice_postcode", "")
#     user_ice.hun_phone = request.POST.get("hun_phone", "")
#     user_ice.save()
#
#
# def update_phone_data(request, user):
#     phone_number = PhoneNumber.objects.get_or_create(user_id=user, id=request.POST.get("phone_id", ""))
#     country = CountryInfo.objects.get(country_name=request.POST.get("country", ""))
#
#     phone_number.number = int(request.POST.get("phone_number", ""))
#     phone_number.name = request.POST.get("phone_name", "")
#     phone_number.country_code = country.country_code
#     phone_number.save()
#
#
# def double_save_personal_data(request, user):
#     user_info, created = UserInfoSave.objects.get_or_create(user=user)
#     user_info.user_name = request.POST.get("username", "")
#     user_info.email = request.POST.get("email", "")
#     user_info.first_name = request.POST.get("firstname", "")
#     user_info.last_name = request.POST.get("lastname", "")
#
#     user_info.home_country = request.POST.get("home_country", "")
#     user_info.home_city = request.POST.get("home_city", "")
#     user_info.home_street = request.POST.get("home_street", "")
#     user_info.home_house_no = request.POST.get("home_house_no", "")
#     user_info.home_postcode = request.POST.get("home_postcode", "")
#     user_info.save()
#
#
# def double_save_abroad_data(request, user):
#     user_info, created = UserInfoSave.objects.get_or_create(user=user)
#     user_info.abroad_country = request.POST.get("abroad_country", "")
#     user_info.abroad_city = request.POST.get("abroad_city", "")
#     user_info.abroad_street = request.POST.get("abroad_street", "")
#     user_info.abroad_house_no = request.POST.get("abroad_house_no", "")
#     user_info.abroad_postcode = request.POST.get("abroad_postcode", "")
#     user_info.save()
#
#
# def double_save_ice_data(request, user):
#     user_ice_save, created = UserIceSave.objects.get_or_create(user=user)
#     user_ice_save.email = request.POST.get("ice_email", "")
#     user_ice_save.firstname = request.POST.get("ice_firstname", "")
#     user_ice_save.lastname = request.POST.get("ice_lastname", "")
#     user_ice_save.country = request.POST.get("ice_country", "")
#     user_ice_save.city = request.POST.get("ice_city", "")
#     user_ice_save.street = request.POST.get("ice_street", "")
#     user_ice_save.house_no = request.POST.get("ice_house_no", "")
#     user_ice_save.postcode = request.POST.get("ice_postcode", "")
#     user_ice_save.hun_phone = request.POST.get("hun_phone", "")
#     user_ice_save.save()
#
#
# def double_save_phone_data(request, user):
#     phone_number = PhoneNumberSave.objects.get_or_create(user_id=user, id=request.POST.get("phone_id", ""))
#     country = CountryInfo.objects.get(country_name=request.POST.get("country", ""))
#
#     phone_number.number = int(request.POST.get("phone_number", ""))
#     phone_number.name = request.POST.get("phone_name", "")
#     phone_number.country_code = country.country_code
#     phone_number.save()