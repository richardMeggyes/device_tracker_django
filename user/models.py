from django.db import models
from django.contrib.auth.models import User


# class UserInfo(models.Model):
#     user = models.OneToOneField(User, on_delete=models.CASCADE)
#
#     home_country = models.CharField(max_length=100, blank=True, null=True)
#     home_city = models.CharField(max_length=100, blank=True, null=True)
#     home_street = models.CharField(max_length=100, blank=True, null=True)
#     home_house_no = models.CharField(max_length=10, blank=True, null=True)
#     home_postcode = models.CharField(max_length=10, blank=True, null=True)
#
#     abroad_country = models.CharField(max_length=100, blank=True, null=True)
#     abroad_city = models.CharField(max_length=100, blank=True, null=True)
#     abroad_street = models.CharField(max_length=100, blank=True, null=True)
#     abroad_house_no = models.CharField(max_length=10, blank=True, null=True)
#     abroad_postcode = models.CharField(max_length=10, blank=True, null=True)
#
#     phone_mobile = models.CharField(max_length=50, blank=True, null=True)
#     phone_work = models.CharField(max_length=50, blank=True, null=True)
#
#     about_me = models.CharField(max_length=5000, blank=True, null=True)
#
#     class Meta:
#         db_table = 'user_info'



# class UserTask(models.Model):
#     user = models.ForeignKey(User, on_delete=models.CASCADE)
#     id = models.AutoField(primary_key=True)
#     is_done = models.BooleanField(default=False, null=False)
#     title = models.CharField(max_length=100, blank=True, null=False)
#     description = models.CharField(max_length=1000, blank=True, null=True)
#     priority = models.IntegerField( blank=True, null=False, default=0)
#     time_added = models.DateTimeField(max_length=100, blank=True, null=True)
#     due_date = models.DateTimeField(max_length=100, blank=True, null=True)
#     completion_date = models.DateTimeField(max_length=30, blank=True, null=True)
#
#     class Meta:
#         db_table = 'user_task'

#
# class PhoneNumber(models.Model):
#     id = models.AutoField(primary_key=True)
#     user = models.ForeignKey(User, on_delete=models.CASCADE)
#     country_code = models.IntegerField(blank=True, null=False, default=0)
#     number = models.CharField(max_length=100, blank=True, null=True)
#     description = models.CharField(max_length=1000, blank=True, null=True)
#     name = models.CharField(max_length=200, blank=True, null=True)
#
#     class Meta:
#         db_table = 'phone_number'


# These are the duplicated tables, to save the user's information every time they edit something.
# This is a prevention of losing user information in case of a legal problem

# class UserInfoSave(models.Model):
#     user = models.OneToOneField(User, on_delete=models.CASCADE)
#
#     user_name = models.CharField(max_length=100, blank=True, null=True)
#     first_name = models.CharField(max_length=100, blank=True, null=True)
#     last_name = models.CharField(max_length=100, blank=True, null=True)
#     email = models.CharField(max_length=100, blank=True, null=True)
#
#     home_country = models.CharField(max_length=100, blank=True, null=True)
#     home_city = models.CharField(max_length=100, blank=True, null=True)
#     home_street = models.CharField(max_length=100, blank=True, null=True)
#     home_house_no = models.CharField(max_length=10, blank=True, null=True)
#     home_postcode = models.CharField(max_length=10, blank=True, null=True)
#
#     abroad_country = models.CharField(max_length=100, blank=True, null=True)
#     abroad_city = models.CharField(max_length=100, blank=True, null=True)
#     abroad_street = models.CharField(max_length=100, blank=True, null=True)
#     abroad_house_no = models.CharField(max_length=10, blank=True, null=True)
#     abroad_postcode = models.CharField(max_length=10, blank=True, null=True)
#
#     phone_mobile = models.CharField(max_length=50, blank=True, null=True)
#     phone_work = models.CharField(max_length=50, blank=True, null=True)
#
#     about_me = models.CharField(max_length=5000, blank=True, null=True)
#
#     class Meta:
#         db_table = 'user_info_save'


# class PhoneNumberSave(models.Model):
#     id = models.AutoField(primary_key=True)
#     user = models.ForeignKey(User, on_delete=models.CASCADE)
#     country_code = models.IntegerField( blank=True, null=False, default=0)
#     number = models.CharField(max_length=100, blank=True, null=True)
#     description = models.CharField(max_length=1000, blank=True, null=True)
#     name = models.CharField(max_length=200, blank=True, null=True)
#
#     class Meta:
#         db_table = 'phone_number_save'


# These are basic information provider tables. I had no better place to put them

# class CountryInfo(models.Model):
#     country_name = models.CharField(max_length=200, blank=True, null=True)
#     country_code = models.CharField(max_length=200, blank=True, null=True)
#     iso_2 = models.CharField(max_length=200, blank=True, null=True)
#     iso_3 = models.CharField(max_length=200, blank=True, null=True)
#     population = models.CharField(max_length=200, blank=True, null=True)
#     area = models.CharField(max_length=200, blank=True, null=True)
#     gdp = models.CharField(max_length=200, blank=True, null=True)
#
#     class Meta:
#         db_table = 'country_info'