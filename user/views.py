import logging, time
from utils import render
from django.contrib.auth.models import User
# from user.models import UserInfo, UserIce, UserTask, PhoneNumber, CountryInfo
from django.contrib.auth.decorators import login_required, permission_required
# from .utils import update_user_data


# @login_required()
# # @permission_required('user.change_userprofile')  # PERMISSION OK
# def user_profile(request):
#     """
#     View function for the user's profile.
#     """
#     user = request.user
#
#     if request.method == 'POST':
#         for x in request.POST:
#             print('POST ITEM', x)
#
#         try:
#             update_user_data(request, user)
#         except Exception as e:
#             print('Exception on user profile:', e)
#
#     user_info, created = UserInfo.objects.get_or_create(user=user)
#     user_ice, created = UserIce.objects.get_or_create(user=user)
#     phone_numbers = PhoneNumber.objects.filter(user=user)
#
#     country_codes = []
#     for phone_number in phone_numbers:
#         country_codes.append(phone_number.country_code)
#
#     country_info = CountryInfo.objects.filter(country_code__in=country_codes)
#
#     for phone_number in phone_numbers:
#         for country in country_info:
#             if str(phone_number.country_code) == country.country_code:
#                 phone_number.img = "/img/flags/" + country.iso_2 + ".png"
#                 break
#             else:
#                 phone_number.img = "/img/flags/not_found.png"
#
#     countries = CountryInfo.objects.all()
#
#     return render(request, 'user/user-profile_MR_copy.html',
#                   {'user_profile': user_info, 'user_ice': user_ice, 'user': user, 'phone_numbers': phone_numbers,
#                    'country_info': country_info, 'countries': countries})
#
#
# @login_required()
# # @permission_required('user.change_userprofile')  # PERMISSION OK
# def user_tasks(request):
#     """
#     View function for the user's tasks page.
#     """
#
#     user = request.user
#     user_profile, created = UserInfo.objects.get_or_create(user=user)
#
#     tasks = UserTask.objects.filter(user=user)
#
#     if request.method == "POST":
#         pass
#         # TODO handle data update
#
#     return render(request, 'user/tasks.html',
#                   {'user_profile': user_profile, 'tasks': tasks})
#
#
# def new_phone_number(request):
#     pass
#
