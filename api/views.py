from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from api.models import DeviceLog
from django.views.decorators.csrf import csrf_exempt

# @login_required()
# @permission_required('user.change_userprofile')
@csrf_exempt
def api_test(request):
    print("REQUEST:", request)
    print("GET:", request.GET)
    print("POST:", request.POST)
    # print("host:", request.get_host())
    print("body:", request.body)
    print("COOKIES:", request.COOKIES)
    return HttpResponse('<head></head>')

@csrf_exempt
def device_log(request):
    post = request.POST
    print(post)
    if len(post) == 0:
        print("got null")
        return JsonResponse({'s': 'ok'})

    data = post['d'].split(',')
    new_log = DeviceLog.objects.create(user=1, device=post['t'],
                                       latitude=float(data[0]),
                                       longitude=float(data[1]),
                                       speed=float(data[2]),
                                       accuracy=float(data[3]),
                                       timestamp=int(data[4]))
    new_log.save()

    return JsonResponse({'r': 'ok'})
