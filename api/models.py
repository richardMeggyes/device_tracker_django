from django.db import models
from django.contrib.auth.models import User
from devices.models import Device


class DeviceLog(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.IntegerField()
    device = models.ForeignKey(Device, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    latitude = models.FloatField()
    longitude = models.FloatField(max_length=200)
    # altitude = models.FloatField(max_length=200)
    # bearing = models.FloatField(max_length=200)
    speed = models.FloatField(max_length=200)
    accuracy = models.FloatField(max_length=200)
    timestamp = models.IntegerField()

    # provider = models.CharField(max_length=200)

    class Meta:
        db_table = 'device_log'
