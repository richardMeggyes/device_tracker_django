from django.urls import path
from . import views
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    path('', csrf_exempt(views.api_test), name='api_test'),
    path('device-log', csrf_exempt(views.device_log), name='device_log')
]
