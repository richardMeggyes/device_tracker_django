# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Company(models.Model):
    company_id = models.AutoField(primary_key=True)
    company_name = models.CharField(max_length=100)
    registration_no = models.CharField(max_length=40, blank=True, null=True)
    country = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    house_no = models.CharField(max_length=30, blank=True, null=True)
    postcode = models.CharField(max_length=20, blank=True, null=True)
    phone_landline = models.CharField(max_length=50, blank=True, null=True)
    phone_mobile = models.CharField(max_length=50, blank=True, null=True)
    partner = models.ForeignKey('Partner', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        db_table = 'company'


class Ice(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    ice_id = models.AutoField(primary_key=True)
    firstname = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100, blank=True, null=True)
    country = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    street = models.CharField(max_length=100, blank=True, null=True)
    house_no = models.CharField(max_length=30, blank=True, null=True)
    hun_phone = models.CharField(max_length=100, blank=True, null=True)
    other_phone = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        db_table = 'ice'


class Partner(models.Model):
    partner_info_id = models.AutoField(primary_key=True)
    partner_name = models.CharField(unique=True, max_length=100)
    score = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'partner'


class PartnerContactPerson(models.Model):
    contact_id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    phone_mobile = models.CharField(max_length=100, blank=True, null=True)
    phone_other = models.CharField(max_length=100, blank=True, null=True)
    partner = models.ForeignKey(Partner, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        db_table = 'partner_contact_person'


class PartnerType(models.Model):
    is_driver = models.BooleanField()
    is_agent = models.BooleanField()
    is_landlord = models.BooleanField()
    partner = models.ForeignKey(Partner, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        db_table = 'partner_type'



class Transfer(models.Model):
    from_airport = models.ForeignKey(Airport, models.DO_NOTHING, db_column='from_airport', blank=True, null=True)
    to_airport = models.ForeignKey(Airport, models.DO_NOTHING, db_column='to_airport', blank=True, null=True)
    outbound_time = models.DateTimeField(blank=True, null=True)
    inbound_time = models.DateTimeField(blank=True, null=True)
    flight_no = models.CharField(max_length=10, blank=True, null=True)
    status = models.CharField(max_length=20, blank=True, null=True)
    submission_time = models.DateTimeField(blank=True, null=True)
    partner = models.ForeignKey(Partner, models.DO_NOTHING, blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    transfer_id = models.AutoField(primary_key=True)

    class Meta:
        db_table = 'transfer'


# class User(models.Model):
#     username = models.CharField(unique=True, max_length=25)
#     password = models.CharField(max_length=100)
#     email = models.CharField(unique=True, max_length=50)
#     salt = models.CharField(max_length=20)
#     role = models.IntegerField(blank=True, null=True)
#     identity = models.CharField(max_length=25, blank=True, null=True)
#
#     class Meta:
#         db_table = 'user'



