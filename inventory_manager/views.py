from utils import render
from django.shortcuts import redirect
from django.contrib.auth.models import Permission
from utils import get_permissions


def index_main(request):
    """
    View function for home page of site.
    """

    if request.user.is_authenticated:
        return render(
            request,
            'index.html',
        )

    else:
        return redirect('login')


def job_detailed(request):
    """
    View function for home page of site.
    """

    return render(request, 'cleaner-template.html')


def transfer(request):
    """
    View function for home page of site.
    """

    return render(request, 'user_transfer/user_transfer.html')